{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# P01c The atmospheric CO$_2$ budget \n",
    "\n",
    "#### Maarten Krol and Michiel van der Molen (version 4.0, August 2021)\n",
    "\n",
    "## Contents\n",
    "\n",
    "1. Introduction: Simulating fossil fuel emissions in MOGUNTIA\n",
    "\n",
    "2. Start concentration and emissions\n",
    "\n",
    "3. Run MOGUNTIA\n",
    "\n",
    "4. CO$_2$ sink\n",
    "\n",
    "5. The global CO$_2$ balance in detail<br>\n",
    "    5.1 The terrestrial biosphere sink<br>\n",
    "    5.2 The ocean sink<br> \n",
    "    5.3 The Biosphere + Ocean sink<br>\n",
    "    5.4 Inverting the land sink<br>\n",
    "    \n",
    "6. Reflection    \n",
    "\n",
    "Before you start, please save the notebook under \n",
    "> File > save as.. > `P01c_*yourname*.ipynb`\n",
    "\n",
    "____\n",
    "\n",
    "\n",
    "## 1 Introduction: Simulating fossil fuel emissions in MOGUNTIA\n",
    "\n",
    "The atmospheric concentration of CO$_2$ has been increasing from about 330 ppm in 1974 to about 414 ppm in 2020 (see figure 1). These data are collected by a dedicated network of atmospheric observation stations: <a href=\"https://www.esrl.noaa.gov/gmd/dv/iadv/\">https://www.esrl.noaa.gov/gmd/dv/iadv/</a> (Check it out, it is an impressive network!). The data in figure 1 contains a wealth of information about the global carbon cycle, but it is not trivial to extract that information. A powerful method is to compare the observed concentrations in the real atmosphere with modeled concentrations, which describe our latest knowledge about the carbon cycle. The degree of comparison may be carefully used as an indication of how well we understand the underlying processes. This is what you will be doing today.\n",
    "\n",
    "<figure>\n",
    "  <img src=\"FIGS/MLO_1969-2019.png\", width=\"600\" height=\"400\">\n",
    "  <figcaption> <i>Figure 1: Atmospheric CO$_2$ concentrations measured at Mauna Loa by NOAA/ESRL, Units are in $\\mu$mol mol$^{-1}$ which is equivalent to ppm.\n",
    "</i></figcaption>\n",
    "</figure>\n",
    "\n",
    "There is little doubt that the anthropogenic CO$_2$ emissions that are related to fossil fuel use are responsible for the increase in atmospheric CO$_2$ concentrations. \n",
    "But what would be the increase in atmospheric concentrations if all the CO$_2$ would remain in the atmosphere? \n",
    "And what is the gradient between the Northern Hemisphere (most emissions) and the Southern hemisphere (less emissions)? To test this, you will implement CO$_2$ as a non-reactive tracer in the MOGUNTIA model, initially with only fossil fuel emissions. Other processes, such as uptake by the biosphere, oceans, and emission as a result of biomass burning, are initially ignored.\n",
    "\n",
    "In order to compare to model to the real atmosphere, measurements taken by NOAA ESRL will be used. The link is given below (see Table 1). Also, the Carbon Dioxide Information Analysis Center provides estimates of world-wide CO$_2$ emissions. The web-addresses are used throughout the exercises.\n",
    "\n",
    "<table>\n",
    "<caption>Table 1: Relevant web-addresses for the exercises</caption>\n",
    "<tr>\n",
    "<td align=\"left\">CO$_2$ measurements made by NOAA ESRL</td>\n",
    "<td align=\"left\"><a href=\"http://www.esrl.noaa.gov/gmd/dv/iadv/\">http://www.esrl.noaa.gov/gmd/dv/iadv/</a></td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"right\">List of observation sites and 3 letter abbreviations</td>\n",
    "<td align=\"right\"><a href=\"https://www.esrl.noaa.gov/gmd/dv/site/\">https://www.esrl.noaa.gov/gmd/dv/site/</a></td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td>Fossil fuel emission estimates</td>\n",
    "<td><a href=\"https://cdiac.ess-dive.lbl.gov/ftp/ndp030/global.1751_2014.ems\">https://cdiac.ess-dive.lbl.gov/ftp/ndp030/global.1751_2014.ems</a></td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td>MOGUNTIA description</td>\n",
    "<td><a href=\"http://www.staff.science.uu.nl/~krol0101/moguntia/MANUAL/moguntia.doc/\">http://www.staff.science.uu.nl/~krol0101/moguntia/MANUAL/moguntia.doc/</a></td>\n",
    "</tr>\n",
    "</table>\n",
    "\n",
    "\n",
    "\n",
    "### Exercise 6:  Fossil fuel emissions in MOGUNTIA\n",
    "In this exercise you will build a global CO$_2$ model and gradually learn how to refine it, so it realistically represents observed CO$_2$ concentrations. Globally, about 10 Pg C are emitted each year due to fossil fuel combustion and deforestation. Let's use MOGUNTIA to simulate the effect of these emissions on the CO$_2$ concentration.\n",
    "\n",
    "<ol>\n",
    "<li>Open the input file CO2_Exercise2.in via the file list tab. \n",
    "<li>In `CO2_Exercise2.in`, set a realistic start concentration, land emissions and life time:\n",
    "    <ul>\n",
    "    <li>In order to make a realistic start with the modelling, you may set the start concentration to 368 ppm in 2000\n",
    "    <li>Implement emission to the equivalent of 10 Pg C yr$^{-1}$ (see MOGUNTIA-02-Introduction.ipynb). \n",
    "    <li>We turn off the exponential/radioactive decay:<br>\n",
    "    `START_CONCENTRATION 368e3`<br>\n",
    "    `EMISSION LAND 9.49e12`<br>\n",
    "    `LIFE_TIME -1`<br>\n",
    "    This will implement CO$_2$ emissions over land into the model. Note that the emissions are the equally distributed over all land pixels, the simulated emissions are not larger over industrial areas than over natural areas.\n",
    "    <li>Save the input file (<CTRL>+S, the disk icon or File--> Save; a * asterisk in the browser tab before the file name means the file is unsaved. Unsaved changes will not take effect).\n",
    "    </ul>\n",
    "<li>Then, initialise the model by typing **SHIFT ENTER** in the Python cell below. \n",
    "<li>It will ask you to select an input file (*.in). Select your modified input file CO2_Exercise2.in.\n",
    "<li>Run MOGUNTIA by clicking `Run Model`. This will now run the MOGUNTIA program. This may take a while. When the simulation is finished, the output will appear as text, or the produced output files will appear in the block `Output`.\n",
    "<li>To show the model output, we use the Python language. Python can import functionality, provides plotting routines, exchange with the operating system, etc. The command below: `pm1 = plot_moguntia()`, starts a model instance, and the variable pm carries the necessary information to the plotting program. You can analyse two runs in two separate instances like\n",
    "\n",
    "            pm1 = plot_moguntia()\n",
    "\n",
    "        and in another Notebook cell:\n",
    "\n",
    "            pm2 = plot_moguntia()\n",
    "            \n",
    "In this way, you can compare the results from two different runs.\n",
    "</ol>\n",
    "Make a figure of the simulated concentrations by selecting:\n",
    "- Output: Exc2.stations\n",
    "- Stations: select all or some stations by clicking and holding **SHIFT** or **CTRL**\n",
    "- Do not click 'overplot' yet. There will be no comparison with measurements yet.\n",
    "- Conversion: ppm\n",
    "- Click 'Make Plots'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "824ce536ecf74064b256456da327af76",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Box(children=(Box(children=(Dropdown(description='Inputfile', options=('CO2_Exercise2.in', 'CO2_Exercise4.in',…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "2379beafe27c4483bf23d6e79e29280b",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Output()"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "93b90166b6024842907293bb0cedf80a",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(ToggleButton(value=False, description='Make Plots'), Output()), _dom_classes=('widget-in…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "from plot_moguntia import *\n",
    "\n",
    "pm2 = plot_moguntia()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will now see a figure of the CO$_2$ concentration from 2000 to 2008 for several locations in the model. There are different types of output (depending on what you prescribed in the input file:\n",
    "- <b>stations</b>: creates time series of concentrations sampled at specified locations. These are <i>modelled</i> time series which you may use to compare with <i>observed</i> time series.\n",
    "- <b>ll</b>: lat-lon maps at a specified height (pressure level). These are useful to locate the major sources and sinks and to see the effect of transport.\n",
    "- <b>za</b>: zonal average: East-West average concentration, it shows concentration figures with latitude on the x-axis and height on the y-axis. \n",
    "- the tickbox **automatic** means that the colour scales will be set automatically. This is often useful, except when you want to see how concentrations are changing in time in a model run with increasing concentrations: all figures will look alike, because only the colour scale will change. In those situations it is better to untick the 'automatic' tickbox and set a fixed scale.<br>\n",
    "\n",
    "Experiment a bit with the different output types. Note: In this practical you will compare the interannual growth rate and the amplitude of the seasonal cycle in the CO$_2$ concentration in different model runs. To keep the overview, create a table with rows for different model runs and columns for description, growth rate and amplitude of the seasonal cycle. Fill out the data each time after you have done a new model run you collected so far.\n",
    "\n",
    "### Question 6.1: How fast does the CO$_2$ concentration typically increase? Is this realistic? Check the MOGUNTIA model results with actual observations at a <a href=\"https://www.esrl.noaa.gov/gmd/dv/iadv/graph.php?code=MLO&program=ccgg&type=ts\">station</a>. What is the growth rate in MOGUNTIA and what is it in reality? Verify your result using a figure and presenting your results in a table.\n",
    "\n",
    "### Question 6.2: Make a figure with zonal average concentrations (Output: Exc2.za.yyyy_mm). What do you observe here? Can you explain your results?\n",
    "\n",
    "### Question 6.3: Where do you see the effect of the ITCZ and how does the CO$_2$ move from the Northern to the Southern hemisphere?\n",
    "\n",
    "### Question 6.4: Make a figure with a lat-lon map of the surface concentrations (Output: Exc2.ll.yyyy_mm). Describe where the ITCZ is in January and July. Can you explain how this affects the CO$_2$ concentration at the Equator?\n",
    "\n",
    "### Question 6.5: Describe in detail how the concentration in Samoa changes in time. The variation in concentration cannot be explained by uptake and release from the biosphere because you only implemented (constant) emissions. Note that this exercise is meant for you to learn to observe and report on your results scientifically. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4 CO$_2$ sink\n",
    "\n",
    "## 4.1 Introduction\n",
    "\n",
    "In Exercise 6 you found that the CO$_2$ growth rate in MOGUNTIA is larger than in reality. This is obviously caused because you only implemented emissions, but no removal processes. Of the 10 Pg C /yr that is emitted, only 4.5 Pg / yr remains in the atmosphere. The other 5.5 Pg C /yr are absorbed by biosphere on land and by the oceans. Let us try to implement a sink in MOGUNTIA. \n",
    "\n",
    "The atmosphere contains about 750 PgC. The relative removal rate $R$ thus equals 5.5 PgC/yr / 750 PgC = 0.00733 / yr (0.73 %/yr).\n",
    "\n",
    "### Exercise 7: CO$_2$ sink in MOGUNTIA\n",
    "\n",
    "### Question 7.1: What is the life time of CO$_2$ (in units of days) in the atmosphere based on the relative removal rate? Check the practical's manual if needed.\n",
    "\n",
    "### Question 7.2: How does the CO$_2$ concentration change when you change the CO$_2$ life time? To change the life time of CO$_2$, make a copy of the input file `CO2_Exercise2.in` and name it `CO2_Exercise3.in`. This way the input file CO2_Exercise2.in will remain intact. Run MOGUNTIA again using the cell box below and inspect how the concentration changes. How does the simulated CO$_2$ growth rate compare with the CO$_2$ growth rate in real observations? Fill out the data in the table of Excercise 6 (Question 6.1).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline\n",
    "from plot_moguntia import *\n",
    "\n",
    "pm2 = plot_moguntia()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The CO$_2$ growth rate in MOGUNTIA looks better now. But it did not improve for the right reason. You will find out in the next exercise.\n",
    "\n",
    "In fact, it is an important scientific discussion whether the terrestrial and oceanic sinks are relative sinks (i.e. the sink rate depends on the atmospheric concentration, eventually resulting in an equilibrium concentration) or an absolute sink (i.e., the earth's surface and the oceans remove a fixed amount of CO$_2$ from the atmosphere, which is independent from the atmospheric concentration). It seems like the sink rate is keeping pace with the increase in the emissions, but no one knows what the underlying mechanisms are. It may take decades before we know.\n",
    "\n",
    "In any case, the quick solution of implementing a life time for CO$_2$ is not very realistic, because it implies that CO$_2$ is removed by chemical processes in all layers in the atmosphere. In reality, CO$_2$ is removed by processes at the earth's surface: the terrestrial biosphere and the oceans' surface.\n",
    "\n",
    "\n",
    "# 5 The global CO$_2$ balance in detail\n",
    "\n",
    "The global CO$_2$ cycle is graphically shown in Figure 2. \n",
    "\n",
    "<figure>\n",
    "  <img src=\"FIGS/co2_budget.png\", width=\"600\" height=\"400\">\n",
    "  <figcaption> <i>Figure 2: Diagram showing the estimated budget for CO$_2$. Blue numbers indicate fluxes in GtC/yr and black numbers indate reservoirs in GtC. 1 GtC corresponds to 10$^9$ 1000 kgC = 10$^{15}$gC = 1 PgC.\n",
    "</i></figcaption>\n",
    "</figure>\n",
    "\n",
    "As a rough estimate, the global atmospheric CO$_2$ mass balance may be written as:\n",
    "<br><br>\n",
    "\\begin{equation*}\n",
    "\\frac{dm_{CO_2,atm}}{dt} = E_{fossil} + E_{LUC}- S_{land} - S_{ocean}\n",
    "\\end{equation*}<br>\n",
    "where:$E_{fossil}$ = 9 Pg C yr$^{-1}$ represents the CO$_2$ emitted by fossil fuel combustion, $E_{LUC}$ = 1 Pg C yr$^{-1}$ is the emissions from land use change (deforestation),  $S_{land}$ = 3.0 Pg C yr$^{-1}$ and \n",
    "$S_{ocean}$ = 2.5 Pg C yr$^{-1}$ are the carbon sinks by the terrestrial biosphere and by the oceans, respectively. As a result the change in CO$_2$ content in the atmosphere, $dm_{CO2,atm}/dt$ = 4.5 Pg C yr$^{-1}$. The CO$_2$ content of the atmosphere thus increases due to the emissions, but the land and ocean sinks cause the growth rate to be halved.\n",
    "\n",
    "\n",
    "## 5.1 The terrestrial biosphere sink\n",
    "\n",
    "We will first inspect the role of the terrestrial biospheric sink.\n",
    "\n",
    "### Question 7.3: Before modifying anything, go back to cell at the beginning of Exercise 6. Now, next to the model results add observations to the output figure by selecting all five observation stations under `Measurem...`, `ppm`, and by clicking on `Overplot` and `Make Plots`. You may extend the simulation period to 2008 for a better comparison. Earlier, you saw that the simulated CO$_2$ time series have a seasonal cycle. However, the observed CO$_2$ time series at the same locations have a much larger seasonal cycle compared to the one simulated in MOGUNTIA. Can you explain this? Which process or processes are you missing? We will remedy this in the next step.\n",
    "\n",
    "Before you continue with question 7.4, do the following: \n",
    "- Open CO2_Exercise4.in\n",
    "- Verify that the exponential loss in the atmosphere has been turned of by resetting LIFE_TIME to -1.\n",
    "- Implement a terrestrial sink by adding the command: SINK FILE co2_sink_bio.dat. This implements a biospheric exchange term in MOGUNTIA, simulating uptake/photosynthesis of CO$_2$ (larger in the summer) and respiration (larger in the winter). The co2_sink_bio.dat prescribes a CO$_2$ sink that depends on location and month.\n",
    "- Run MOGUNTIA (using the cell box below) with your updated input file CO2_Exercise4.in. Inspect the simulated CO$_2$ time series at five stations (make a selection of stations at the NH, SH and in the tropics, see Table 1 for a link to the station codes), compared to the observed ones. You may want to run the model until 2008 to improve the comparison with the observations.\n",
    "\n",
    "### Question 7.4:  Did the amplitude of the seasonal cycle change? Can you explain?\n",
    "    \n",
    "### Question 7.5: What is the CO$_2$ growth rate in this experiment? Fill out the data in the table of Excercise 6 (Question 6.1)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline\n",
    "from plot_moguntia import *\n",
    "\n",
    "pm1 = plot_moguntia()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## 5.2 The ocean sink\n",
    "\n",
    "In the previous section, you have seen that MOGUNTIA simulated concentrations gradually start to look like the observated concentrations in the real atmosphere. However, the CO$_2$ growth rate in MOGUNTIA is still larger than in reality. This may not be a complete surprise, because you are still missing an important process in the model: uptake of CO$_2$ by the oceans.\n",
    "\n",
    "Implementing the ocean sink is quite simple. In `CO2_Exercise4.in` you should replace SINK FILE co2_sink_bio.dat by SINK FILE co2_sink_oc.dat. Now, go to the cell box below and run your model again by pressing **Shift Enter**, choose `CO2_Exercise4.in` as your `Inputfile`, and then answer the following questions:\n",
    "\n",
    "### Question 7.6: Run MOGUNTIA with your updated input file `CO2_Exercise4.in`. Inspect the simulated CO$_2$ time series at the five stations and compare the results to the observed time series. Did the amplitude of the seasonal cycle change? Can you explain your answer?\n",
    "\n",
    "### Question 7.7: What is the CO$_2$ growth rate in this experiment? And how large is the amplitude of the seasonal cycle? Fill out the data in the table of Excercise 6 (Question 6.1)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline\n",
    "from plot_moguntia import *\n",
    "\n",
    "pm1 = plot_moguntia()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5.3 The Biosphere + Ocean sink\n",
    "\n",
    "In the previous section, you have seen that the MOGUNTIA simulated concentrations gradually start to look like the observated concentrations in the real atmosphere. However, the CO$_2$ growth rate in MOGUNTIA is still larger than in reality. This may not be a complete surprise, because you are still missing an important process in the model: uptake of CO$_2$ by the biospehere and the ocean. Implementing this type sink is quite simple. You should again open `CO2_Exercise4.in` and replace SINK FILE co2_sink_oc.dat by SINK FILE co2_sink_ocbio.dat Now, go to the cell box below and run your model again by pressing **Shift Enter**, choose `CO2_Exercise4.in` as your `Inputfile`, and then answer the following questions:\n",
    "\n",
    "### Question 7.8: Run MOGUNTIA with your updated input file `CO2_Exercise4.in`. Inspect the simulated CO$_2$ time series at the five stations and compare the results to the observed time series. Did the amplitude of the seasonal cycle change? Can you explain your answer?\n",
    "\n",
    "### Question 7.9: What is the CO$_2$ growth rate in this experiment? How large is the amplitude of the seasonal cycle? Fill out the data in the table of Excercise 6 (Question 6.1).\n",
    "\n",
    "### Question 7.10: Inspect the zonal average CO$_2$ concentrations. Is there a seasonal pattern in the interhemispheric CO$_2$ gradient? How is this different than in the run with only biospheric exchange or with only the ocean sink? Use appropriate figures to explain your answer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline\n",
    "from plot_moguntia import *\n",
    "\n",
    "pm1 = plot_moguntia()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5.4 Inverting the land sink\n",
    "\n",
    "In the previous exercises, you gradually implemented a realistic carbon mass balance for the atmosphere in MOGUNTIA. You also compared the modeled concentrations with observations from the real atmosphere. In the last exercise, you could see that the modeled concentrations look quite realistic. This gives confidence that the processes we implemented in the model are right and in the correct order of magnitude. It also provides a lot of information about how large the relative processes are relative to each other. \n",
    "\n",
    "However, the match between observed and modeled concentrations is still not that good. Particularly, the growth rate is still off. In practise, the land sink is the most uncertain term in the CO$_2$ mass balance. Since we are quite confident that we implemented the most important terms in the mass balance correctly, we can now try to estimate the magnitude of the terrestrial biosphere sink by minimising the difference between the observed and modeled concentrations. This process is so-called inverse modelling. It is very advanced statistical method. To do this, here, we apply a simple trial-and-error method. You should again open `CO2_Exercise4.in` and add an extra line `SINK EXTRA_LAND 1` to define an extra uptake of CO$_2$ by the land biosphere. This statement in the input file will enhance the land sink by 1 PgC yr$^{-1}$. You may change the number, in order to obtain the best possible fit.\n",
    ". Now, go to the cell box below and run your model again by pressing **Shift Enter**, choose `CO2_Exercise4.in` as your `Inputfile`, and then answer the following questions:\n",
    "\n",
    "### Question 7.11: What is the magnitude of the land sink that leads to the \"best\" match with observations? Use an appropriate figure to verify your answer.\n",
    "    \n",
    "### Question 7.12: How certain are you about the value of the inverted land sink? You started with a model that had a constant concentration. Explain which steps you took to make the model realistic. List these staps and write down what are the strong and weak points in the values of each taken step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline\n",
    "from plot_moguntia import *\n",
    "\n",
    "pm1 = plot_moguntia()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6 Reflection \n",
    "\n",
    "Use following questions to reflect on your work of this practical:\n",
    "\n",
    "- You started this practical with an “empty” MOGUNTIA. You subsequently implemented a more or less realistic CO$_2$ mass balance. Summarise how you did this. What are the various terms in the mass balance and what is the effect of implementing the various terms?\n",
    "    \n",
    "- Reflect on the quality of your final model. What are its strong aspects and what could be improved? How would you answer the question if the land and ocean sinks would be proportional to the atmospheric CO$_2$ concentration (if they would be relative in stead of absolute sinks)?\n",
    "    \n",
    "- In 5-10 lines, reflect briefly on your personal performance during this practical. What did you learn? Are there parts that you would like to study in detail? Do you think that the working methods we presented were suitable for this practical? Reflect on the quality of your own work and write about your personal strengths and weaknesses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
