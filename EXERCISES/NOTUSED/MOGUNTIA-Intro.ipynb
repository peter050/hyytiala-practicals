{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction into MOGUNTIA\n",
    "\n",
    "#### Maarten Krol and Michiel van der Molen (version 5.0, August 2021)\n",
    "#### Wouter Peters (version 5.1, May 2023)\n",
    "----------\n",
    "In this notebook you will learn how to configure MOGUNTIA as a global air quality model. Here, you will do a simulation that uses the concept of lifetime in a situation with constant emissions and a constant relative loss rate. The exerices you do here will serve as a preparation for the next notebooks, where you will use MOGUNTIA to simulate the global atmospheric radon, interhemispheric exchange time, and CO$_2$ balance.\n",
    "____\n",
    "\n",
    "## 1 MOGUNTIA solves the budget equation\n",
    "\n",
    "Atmospheric Composition is simulated in MOGUNTIA based on a budget equation:\n",
    "\n",
    "\\begin{equation*}\n",
    "\\frac{dm}{dt} = E - D + P - L + {\\rm Advection} + {\\rm Turbulent\\ Diffusion},\n",
    "\\end{equation*}\n",
    "where $dm/dt$ is the rate of change of the mass in time, $E$ is the emission, $D$ is the deposition (at the Earth's surface), $P$ is the chemical production and $L$ is the chemical loss. $\\rm Advection$ is the process where the species increases because the wind brings air with higher concentration, and $\\rm Turbulent \\ Diffusion$ is the process where the concentration increases when air in the grid cell is mixed with air with higher concentrations. The advection and diffusion terms can be negative too. They are handled by MOGUNTIA itself. But you need to specify the $E$, $D$, $P$ and $L$ terms in an input file. MOGUNTIA reads the input file and solves the budget equation by integrating it over time for each grid cell individually. Since MOGUNTIA is a spatially explicit model, you may vary the emissions across the globe.\n",
    "\n",
    "## 2 Configuring MOGUNTIA\n",
    "\n",
    "Running MOGUNTIA is basically solving (i.e., integrating) the budget equation in time. Many model aspects are already prepared in MOGUNTIA, such as the grid sizes, the winds and how winds and turbulence mix air and its components (advection and diffusion). The things you need to tell MOGUNTIA are:\n",
    "\n",
    "- which period to run (START_DATE, END_DATE),\n",
    "- what component is simulated (NAME, MOLMASS, START_CONCENTRATION),\n",
    "- how the component is emitted (EMISSION from SEA or LAND and its rate),\n",
    "- how this component decays (LIFE_TIME) or\n",
    "- how it is produced or destroyed by chemical reactions with another component (like OH),\n",
    "- how you want to show the results (OUTPUT).\n",
    "\n",
    "So, we need to specify 10 run characteristics, that must be doable. You need to write and/or edit the input file, that is available to tell MOGUNTIA how you want to configure it. This will be explained in the next section.\n",
    "\n",
    "\n",
    "## 3 The input file and its syntax\n",
    "\n",
    "Let us open the input file to inspect what it does. \n",
    "\n",
    "### Exercise 2: The file syntax\n",
    "\n",
    "- return to the file exporer tab (the window that opened when you first logged in).\n",
    "- open the input file intro.in.\n",
    "- you will see that the input file specifies:\n",
    "   - **run characteristics**: TITLE, START_DATE and END_DATE\n",
    "   - **species characteristics** : NAME (of the simulated species), its MOLMASS, START_CONCENTRATION (ppb), LIFE_TIME (-1 means no chemical loss) and the EMISSION rate (molecules/cm2/s).\n",
    "   - **output specification**: the model outputs monthly concentrations at a few selected *stations* and monthly *latlon* maps (at 1000 hPa, near the surface) and *zonal averages*.\n",
    "   - if a line starts with a *space*, MOGUNTIA will explain it as a *comment* and not read it.\n",
    "   - benefit from automatic highlighting comments by starting them with # (it will improve the readability of the input file)\n",
    "   \n",
    "A detailed description of the input options is given <a href=\"http://www.staff.science.uu.nl/~krol0101/moguntia/MANUAL/moguntia.doc/\">here</a>. Take some 10 minutes to browse through the input file and find out what the commands mean.\n",
    "\n",
    "### Question 2.1: What is the start concentration and what are the intrinsic units for concentration in MOGUNTIA? Make sure that you set the start concentration to 0 ppb for now.\n",
    "\n",
    "### Question 2.2: What are the options for specifying loss processes?\n",
    "\n",
    "### Question 2.3: The EMISSION command is not included in the input file yet. Check out its syntax in the MOGUNTIA manual, so you can include it in the next section. Explain how you can include the emission of 10 Pg C yr$^{-1}$. \n",
    "\n",
    "### Question 2.4: The input file specifies 4 types of outputs (i.e., figures). Those are STATION_OUTPUT, STATION name, OUTPUT MONTHLY LATLON and OUTPUT MONTHLY ZONAL_AVERAGE. Lookup the commands in the MOGUNTIA manual and figure out what the commands mean."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4 Running MOGUNTIA\n",
    "\n",
    "If you understand what the input file means, you can proceed with running the MOGUNTIA model. The steps below explain how to do this.\n",
    "\n",
    "1. To run the model and to show the model output, we use a Python interface. The command below: \"pm1 = plot_moguntia()\", opens a menu to ask for the input file, starts the model instance after pressing 'Run Model' and saves the model output in the variable pm1. The output is then plotted with Python. Note that the model itself is run in Fortran, which is much faster than Python, but not as interactive.\n",
    "1. Go to the cell below and press **SHIFT** + **ENTER**.\n",
    "1. Select the **inputfile**, the dropdown menu lists the present input files  (*.in). Select intro.in.\n",
    "1. Run MOGUNTIA by clicking **Run Model**. This will now run the MOGUNTIA model (solving the budget equation in time), depending on the description you gave in the input file. This may take a short while. When the simulation is finished, the requested output files will appear in the block **Output**:\n",
    "    - Exc2..stations: creates time series of concentrations sampled at specified locations. These are modelled time series which you may use to compare with observed time series.\n",
    "    - Exc2.ll_yyyy_mm: lat-lon maps at a specified height (pressure level). These are useful to locate the major sources and sinks and to see the effect of transport.\n",
    "    - Exc2.za_yyyy_mm: zonal average: East-West average concentration, it shows concentration figures with latitude on the x-axis and height on the y-axis.\n",
    "    - the tickbox 'automatic' means that the colour scales will be set automatically. This is often useful, except when you want to see how concentrations are changing in time in a model run with increasing concentrations: all figures will look alike, because only the colour scale will change. In those situations it is better to untick the 'automatic' tickbox and set a fixed scale. Experiment a bit with the different output types.\n",
    "\n",
    "1. Select the units in the figures (ppm is conventional for CO2).\n",
    "1. Select the outputs you want (start with stations), and press **Make Plots**.\n",
    "1. The overplot function will be explained later.\n",
    "\n",
    "N.B.: The lat-lon and zonal average plots fail at this time, because Python cannot draw contours since there are no concentration differences yet. This will come later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from plot_moguntia import *\n",
    "\n",
    "pm1 = plot_moguntia()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 3: The time series\n",
    "    \n",
    "Follow the procedure described above and make the time series figure (Exc2..stations). \n",
    "\n",
    "### Question 3.1: Verify that the concentration is 0 ppb at all times and at all stations. \n",
    "\n",
    "The concentration is 0 ppb at all stations and all times because you set the START_CONCENTRATION at 0e3 ppb = 0 ppm and you did not implement any emissions yet.\n",
    "\n",
    "N.B.: The latlon and zonal average plots fail at this time, because Python cannot draw contours if there are no concentration differences. This will come later.\n",
    "\n",
    "### Question 3.2: What will happen if you set the START_CONCENTRATION to e.g., 417 ppm? Do this in the input file, save the input file and run the model again.\n",
    "\n",
    "N.B.: sometimes you may see small concentration changes (e.g., 0.010 ppm per year, which arise from numerical diffusion in the model, this is not something to pay attention to)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5 Implementing emissions\n",
    "\n",
    "As in the lecture, we will implement an emission of 10 Pg C yr$^{-1}$ into the atmosphere, which is a realistic value.\n",
    "\n",
    "For this purpose we will use the EMISSION command. Check out the description in <a href=\"http://www.staff.science.uu.nl/~krol0101/moguntia/MANUAL/moguntia.doc/\">http://www.staff.science.uu.nl/~krol0101/moguntia/MANUAL/moguntia.doc/</a>. Considering that the vast majority of the CO$_2$ is emitted over land, we will use the syntax\n",
    "\n",
    "`EMISSION LAND eeeee` \n",
    "\n",
    "where eeeee represents the total emissions from land in molecules cm$^{-2}$ s$^{-1}$. So we need to convert 10 Pg C yr$^{-1}$ to molecules cm$^{-2}$ s$^{-1}$:\n",
    "\n",
    "- 10 PgC = 10 $\\cdot$ 10$^{15}$ g C = 10 $\\cdot$ 10$^{15}$ / (12.0 g C/mol C) = 833$\\cdot$10$^{12}$ mole C\n",
    "- 833$\\cdot$10$^{12}$ mole C = 833$\\cdot$10$^{12}$ mole C * 6.023$\\cdot$10$^{23}$ molecules/mole = 5.017$\\cdot$10$^{38}$ molecules C\n",
    "- The earth's surface area $A = 510.1 \\cdot 10^{6}$ km$^2 = 510.1 \\cdot 10 ^{6}$ km$^2$ * (10$^{6}$ m$^2$ km$^{-2}$) * (10$^{4}$ cm$^{2}$ m$^{-2}$) = 510.1$\\cdot$10$^{16}$ cm$^2$\n",
    "- t = 1 yr = 1 yr * 360 days/yr *24 hours/day *3600 seconds/day = 31104000 s (MOGUNTIA works with 12 months of 30 days = 360 days) \n",
    "- EMISSION = 5.017e38 molecules C/ (510.1e16 cm$^2$) / (31104000 s) = 3.16e12 molecules cm$^{-2}$ s$^{-1}$\n",
    "- However, the emissions are concentrated over land (~1/3 of the Earth's surface), so they are three times larger there and 0 over sea: `EMISSION LAND 9.49e12`.\n",
    "- This will implement CO$_2$ emissions over land into the model. Note that the emissions are equally distributed over all land pixels, the simulated emissions are not larger over industrial areas than over natural areas.\n",
    "\n",
    "### Exercise 4: Emission over land\n",
    "\n",
    "Implement the line `EMISSION LAND 9.49e12` in the input file `intro.in`. Run the model again (i.e., execute the cell below) and make a new version of the time series figure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from plot_moguntia import *\n",
    "\n",
    "pm1 = plot_moguntia()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Question 4.1: How does the concentration increase in time; linearly, exponentially or does it increase towards an equilibrium? Use an appropriate figure to substantiate your answer and explain what you see on that figure.\n",
    "\n",
    "### Question 4.2: Inspect lat-lon figures (i.e., Exc2.ll.yyyy_mm output files) and a few zonal average figures (i.e., Exc2.za.yyyy_mm output files). Why does the concentration change between different locations and altitudes? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5 Final comments\n",
    "\n",
    "In this practical you learned to operate the MOGUNTIA model, how to work with the input file and how to produce output plots. In the next practicals, you will learn how to make further simulations, including the global CO2 budget.\n",
    "\n",
    "If you feel comfortable working with MOGUNTIA, you are ready for the next part of practical, otherwise it is better to experiment a bit more with the input file and test how it affects the resulting concentrations.\n",
    "\n",
    "A few tips for MOGUNTIA input files:\n",
    "- Make only small changes at the time, then try running MOGUNTIA. If you make many changes at the same time, you won't know where you made a mistake in case you made one.\n",
    "- Keep the file organised in all sections.\n",
    "- You may add sampling stations, but be aware that the station name should be written entirely without spaces.\n",
    "- After changing the input file, save it, select the input file and run MOGUNTIA again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
